package prasetyo.dendy.utsmobile

import android.content.Intent
import android.graphics.Color
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_maps.*
import mumayank.com.airlocationlibrary.AirLocation

class MapsActivity : AppCompatActivity(), View.OnClickListener, OnMapReadyCallback {
    val COLLECTION = "mahasiswa"
    val F_ALAMAT = "alamat"
    val F_ID = "id"
    val F_NAMA = "nama"
    var docId = ""
    val PARE = "Pare"
    lateinit var db: FirebaseFirestore
    lateinit var alStudent: java.util.ArrayList<HashMap<String, Any>>
    lateinit var adapter: SimpleAdapter

    var lat : Double = 0.0; var lng : Double = 0.0;
    var airLoc : AirLocation? = null
    var gMap : GoogleMap? = null
    lateinit var mapFragment: SupportMapFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        mapFragment = supportFragmentManager.findFragmentById(R.id.mapsFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        btnGoToPare.setOnClickListener(this)
        lsData2.setOnItemClickListener(itemClick)
        alStudent = java.util.ArrayList()
        fab.setOnClickListener(this)
    }


    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message.toString())
            showData()
        }
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alStudent.get(position)
        edAlamat2.setText(hm.get(F_ALAMAT).toString())

    }


    fun showData() {
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for (doc in result) {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, doc.get(F_ID).toString())
                hm.set(F_NAMA, doc.get(F_NAMA).toString())
                hm.set(F_ALAMAT, doc.get(F_ALAMAT).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(
                this, alStudent, R.layout.row_data,
                arrayOf(F_ID, F_NAMA, F_ALAMAT),
                intArrayOf(R.id.txId, R.id.txNama, R.id.txAlamat)
            )
            lsData2.adapter = adapter
        }
    }

    val MAPBOX_TOKEN = "pk.eyJ1IjoiZGlha3NpenoiLCJhIjoiY2tmdWhjcWpzMTZxcDJ5bnc1Y2prczd0dCJ9.m1ndjbkajDNTl2Zrr0PcoA"
    var URL = ""
    override fun onClick(v: View?) {
        var tempat = edAlamat2.text.toString()
        when(v?.id){
            R.id.btnGoToPare -> {
                URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/" +
                        "$tempat.json?proximity=$lng,$lat&access_token=$MAPBOX_TOKEN&limit=1"
                getDestinationLocation(URL)
            }
            R.id.fab -> {
                val ll = LatLng(lat,lng)
                gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                txMyLocation.setText("My Position : LAT=$lat, LNG=$lng")
            }
        }
    }

    //error ga iso import
    fun getDestinationLocation(url : String){
        val request = JsonObjectRequest(Request.Method.GET,url,null,
            Response.Listener {
                val features = it.getJSONArray("features").getJSONObject(0)
                val place_name = features.getString("place_name")
                val center = features.getJSONArray("center")
                val lat = center.get(0).toString()
                val lng = center.get(1).toString()
                getDestinationRoutes(lng,lat,place_name)
            }, Response.ErrorListener {
                Toast.makeText(this, "can't get location", Toast.LENGTH_SHORT).show()
            })
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    //this fun is used to get the route to the destination
    fun getDestinationRoutes(destLat : String, destLng : String, place_name : String){
        URL = "https://api.mapbox.com/directions/v5/mapbox/driving/" +
                "$lng,$lat;$destLng,$destLat?access_token=$MAPBOX_TOKEN&geometries=geojson"
        val request = JsonObjectRequest(Request.Method.GET,URL,null,
            Response.Listener {
                val routes = it.getJSONArray("routes").getJSONObject(0)
                val legs = routes.getJSONArray("legs").getJSONObject(0)
                val distance = legs.getInt("distance")/1000.0
                val duration = legs.getInt("duration")/60
                txMyLocation.setText("My Location :\nLat : $lat   Lng : $lng\n" +
                        "Destination : $place_name\nLat : $destLat   Lng : $destLng\n" +
                        "Distance : $distance km   Duration : $duration minute")
                val geometry = routes.getJSONObject("geometry")
                val coordinates = geometry.getJSONArray("coordinates")
                var arraySteps = ArrayList<LatLng>()
                for(i in 0..coordinates.length()-1){
                    val lngLat = coordinates.getJSONArray(i)
                    val fLng = lngLat.getDouble(0)
                    val fLat = lngLat.getDouble(1)
                    arraySteps.add(LatLng(fLat,fLng))
                }
                drawRoutes(arraySteps,place_name)
            },
            Response.ErrorListener {
                Toast.makeText(this, "Something is wrong! ${it.message.toString()}",
                    Toast.LENGTH_SHORT).show()
            })
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    //this fun is used to draw route on the map
    fun drawRoutes(array: ArrayList<LatLng>, place_name: String){
        gMap?.clear()
        val polyline = PolylineOptions().color(Color.BLUE).width(10.0f)
            .clickable(true).addAll(array)
        gMap?.addPolyline(polyline)
        val ll = LatLng(lat,lng)
        gMap?.addMarker(MarkerOptions().position(ll).title("Hei, I'm Here"))
        gMap?.addMarker(MarkerOptions().position(array.get(array.size-1)).title(place_name))
        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,10.0f))
    }

    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        if (gMap != null){
            airLoc = AirLocation(this, true,true,
                object : AirLocation.Callbacks{
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(this@MapsActivity, "Failed to get current location", Toast.LENGTH_SHORT).show()
                        txMyLocation.setText("Failed to get current location")
                    }

                    override fun onSuccess(location: Location) {
                        lat = location.latitude; lng = location.longitude
                        val ll = LatLng(location.latitude,location.longitude)
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Hei Im Here"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                        txMyLocation.setText("My Position : LAT=${location.latitude}, "+
                                "LNG=${location.longitude}")
                    }
                })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode,resultCode,data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
